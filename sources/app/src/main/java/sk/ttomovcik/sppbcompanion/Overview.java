package sk.ttomovcik.sppbcompanion;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class Overview extends Fragment
{
    public Overview()
    {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        Objects.requireNonNull(getActivity()).getWindow().setTitle("Prehľad");
        return inflater.inflate(R.layout.fragment_overview,
                container, false);
    }
}
